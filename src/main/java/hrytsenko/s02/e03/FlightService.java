package hrytsenko.s02.e03;

import java.util.List;
import java.util.Optional;

public class FlightService {

    /**
     * Search flight with the minimum number of stops.
     * 
     * @param from
     *            the airport for departure.
     * @param to
     *            the airport for arrival.
     * @param flights
     *            the list of available flights.
     * 
     * @return the flight with the minimum number of stops.
     */
    public Optional<Flight> search(String from, String to, List<Flight> flights) {
        throw new UnsupportedOperationException();
    }

}
