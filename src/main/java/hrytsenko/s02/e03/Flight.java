package hrytsenko.s02.e03;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Objects;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import com.google.common.base.Preconditions;

public class Flight {

    private final String from;
    private final String to;

    private final List<Flight> flights;

    public Flight(String from, String to, List<Flight> flights) {
        this.from = from;
        this.to = to;
        this.flights = new ArrayList<>(flights);
    }

    public String from() {
        return from;
    }

    public String to() {
        return to;
    }

    public List<Flight> flights() {
        return flights.isEmpty() ? Arrays.asList(this) : new ArrayList<>(flights);
    }

    public List<String> stops() {
        List<String> stops = new ArrayList<>();
        stops.add(from);
        flights().stream().map(Flight::to).forEach(stops::add);
        return stops;
    }

    public Flight append(Flight other) {
        Preconditions.checkArgument(Objects.equals(to(), other.from()));
        List<Flight> flights = Stream.of(this, other).map(Flight::flights).flatMap(List::stream)
                .collect(Collectors.toList());
        return new Flight(from(), other.to(), flights);
    }

}
