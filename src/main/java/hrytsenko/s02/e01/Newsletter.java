package hrytsenko.s02.e01;

import java.util.List;

public class Newsletter {

    private String topic;
    private List<String> subscribers;

    public String getTopic() {
        return topic;
    }

    public void setTopic(String topic) {
        this.topic = topic;
    }

    public List<String> getSubscribers() {
        return subscribers;
    }

    public void setSubscribers(List<String> subscribers) {
        this.subscribers = subscribers;
    }

    public boolean hasSubscriber(String subscriber) {
        return subscribers.contains(subscriber);
    }

}
