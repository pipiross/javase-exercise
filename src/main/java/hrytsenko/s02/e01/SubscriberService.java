package hrytsenko.s02.e01;

import java.util.List;
import java.util.Map;

public final class SubscriberService {

    /**
     * Find topics for each subscriber.
     * 
     * @param newsletters
     *            the list of newsletters.
     * 
     * @return the topics for each subscriber.
     */
    public Map<String, List<String>> findTopics(List<Newsletter> newsletters) {
        throw new UnsupportedOperationException();
    }

}
