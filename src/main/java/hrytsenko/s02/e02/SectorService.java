package hrytsenko.s02.e02;

import java.math.BigDecimal;
import java.util.List;
import java.util.Map;

public class SectorService {

    /**
     * Calculate weights of sectors.
     * 
     * <p>
     * Weight represents the capitalization of the companies in the sector in relation to the total capitalization of
     * all companies.
     * 
     * @param companies
     *            the list of companies.
     * 
     * @return the weight by sector.
     */
    public Map<String, BigDecimal> calculateWeights(List<Company> companies) {
        throw new UnsupportedOperationException();
    }

}
