package hrytsenko.s01.e03;

public final class Cards {

    private Cards() {
    }

    /**
     * Validate the card number using the Luhn algorithm:
     * 
     * <ol>
     * <li>Multiply each digit of the card number in the odd position (counting from the right) by 2.</li>
     * <li>Decrease the result of multiplication by 9, if it greater than or equal to 10.</li>
     * <li>If the sum of all digits is multiple of 10, then the card number is valid.</li>
     * </ol>
     * 
     * @param number
     *            the card number.
     * 
     * @return <code>true</code> if the card number is valid and <code>false</code> otherwise.
     * 
     * @throws IllegalArgumentException
     *             If the number of digits is less than 14, or the number of digits is greater than 16, or if the card
     *             number contains non-digits.
     */
    public static boolean validate(String number) {
        throw new UnsupportedOperationException();
    }

}
