package hrytsenko.s01.e01;

public final class Anagrams {

    private Anagrams() {
    }

    /**
     * Check that words are anagrams.
     * 
     * @param first
     *            the first word.
     * @param second
     *            the second word.
     * 
     * @return <code>true</code> if words are anagrams and <code>false</code> otherwise.
     */
    public static boolean validate(String first, String second) {
        throw new UnsupportedOperationException();
    }

}
