package hrytsenko.s01.e01;

public final class Palindromes {

    private Palindromes() {
    }

    /**
     * Check that word is palindrome.
     * 
     * @param word
     *            the word.
     * 
     * @return <code>true</code> if word is palindrome and <code>false</code> otherwise.
     */
    public static boolean validate(String word) {
        throw new UnsupportedOperationException();
    }

}
