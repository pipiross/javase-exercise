package hrytsenko.s01.e02;

public final class Brackets {

    private Brackets() {
    }

    /**
     * Check that following brackets are paired: (), [], {}.
     * 
     * @param text
     *            the text.
     * 
     * @return <code>true</code> if the input text is valid and <code>false</code> otherwise.
     */
    public static boolean validate(String text) {
        throw new UnsupportedOperationException();
    }

}
