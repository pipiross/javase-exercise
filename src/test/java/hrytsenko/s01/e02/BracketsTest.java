package hrytsenko.s01.e02;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

import org.junit.Test;

public class BracketsTest {

    @Test
    public void emptyString() {
        assertTrue(Brackets.validate(""));
    }

    @Test
    public void noBrackets() {
        assertTrue(Brackets.validate("foo"));
    }

    @Test
    public void roundBrackets() {
        assertTrue(Brackets.validate("(foo)"));
    }

    @Test
    public void squareBrackets() {
        assertTrue(Brackets.validate("[foo]"));
    }

    @Test
    public void curlyBrackets() {
        assertTrue(Brackets.validate("{foo}"));
    }

    @Test
    public void nestedBrackets() {
        assertTrue(Brackets.validate("(foo{bar})"));
    }

    @Test
    public void severalBrackets() {
        assertTrue(Brackets.validate("(foo){bar}"));
    }

    @Test
    public void closingBracketIsMissed() {
        assertFalse(Brackets.validate("(foo"));
    }

    @Test
    public void openingBracketIsMissed() {
        assertFalse(Brackets.validate("foo)"));
    }

    @Test
    public void bracketsNotMatched() {
        assertFalse(Brackets.validate("(foo]"));
    }

    @Test
    public void bracketsNotOrdered() {
        assertFalse(Brackets.validate("(foo[bar)]"));
    }

}
