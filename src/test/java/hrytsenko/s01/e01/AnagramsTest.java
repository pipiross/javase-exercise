package hrytsenko.s01.e01;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

import org.junit.Test;

public class AnagramsTest {

    @Test
    public void sameLetters_sameLength() {
        assertTrue(Anagrams.validate("caller", "recall"));
    }

    @Test
    public void sameLetters_differentLength() {
        assertFalse(Anagrams.validate("reset", "rest"));
    }

    @Test
    public void differentLetters_sameLength() {
        assertFalse(Anagrams.validate("race", "rate"));
    }

}
