package hrytsenko.s01.e01;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

import org.junit.Test;

public class PalindromesTest {

    @Test
    public void correctWord() {
        assertTrue(Palindromes.validate("racecar"));
    }

    @Test
    public void incorrectWord() {
        assertFalse(Palindromes.validate("invalid"));
    }

}
