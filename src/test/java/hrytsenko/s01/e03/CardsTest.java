package hrytsenko.s01.e03;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

import org.junit.Test;

import hrytsenko.s01.e03.Cards;

public class CardsTest {

    @Test
    public void visa() {
        assertTrue(Cards.validate("4485312678642542"));
    }

    @Test
    public void visaElectron() {
        assertTrue(Cards.validate("4913718053571357"));
    }

    @Test
    public void masterCard() {
        assertTrue(Cards.validate("5405827821668072"));
    }

    @Test
    public void americanExpress() {
        assertTrue(Cards.validate("371185987578861"));
    }

    @Test
    public void incorrectNumber() {
        assertFalse(Cards.validate("1111111111111111"));
    }

}
