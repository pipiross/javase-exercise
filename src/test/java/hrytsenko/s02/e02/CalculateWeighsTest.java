package hrytsenko.s02.e02;

import static java.util.Arrays.asList;
import static org.hamcrest.CoreMatchers.equalTo;
import static org.hamcrest.CoreMatchers.is;
import static org.junit.Assert.assertThat;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.Map;

import org.junit.Before;
import org.junit.Test;

public class CalculateWeighsTest {

    private static final String TECHNOLOGY = "Technology";
    private static final String CONSUMER_SERVICES = "Consumer Services";

    private static final Company AAPL = company("AAPL", TECHNOLOGY, 600.00);
    private static final Company AMZN = company("AMZN", CONSUMER_SERVICES, 400.00);
    private static final Company MSFT = company("MSFT", TECHNOLOGY, 500.00);

    private SectorService service;

    @Before
    public void init() {
        service = new SectorService();
    }

    @Test
    public void oneSector_oneCompany() {
        Map<String, BigDecimal> weightings = service.calculateWeights(asList(AAPL));

        assertWeight(weightings.get(TECHNOLOGY), 1.00);
    }

    @Test
    public void oneSector_severalCompanies() {
        Map<String, BigDecimal> weightings = service.calculateWeights(asList(AAPL, MSFT));

        assertWeight(weightings.get(TECHNOLOGY), 1.00);
    }

    @Test
    public void severalSectors_oneCompany() {
        Map<String, BigDecimal> weightings = service.calculateWeights(asList(AAPL, AMZN));

        assertWeight(weightings.get(TECHNOLOGY), 0.60);
        assertWeight(weightings.get(CONSUMER_SERVICES), 0.40);
    }

    @Test
    public void severalSectors_severalCompanies() {
        Map<String, BigDecimal> weightings = service.calculateWeights(asList(AAPL, AMZN, MSFT));

        assertWeight(weightings.get(TECHNOLOGY), 0.73);
        assertWeight(weightings.get(CONSUMER_SERVICES), 0.27);
    }

    private static Company company(String symbol, String sector, double marketCap) {
        Company company = new Company();
        company.setSymbol(symbol);
        company.setSector(sector);
        company.setMarketCap(decimal(marketCap));
        return company;
    }

    private static BigDecimal decimal(double value) {
        return BigDecimal.valueOf(value).setScale(2, RoundingMode.HALF_UP);
    }

    private static void assertWeight(BigDecimal actualWeight, double expectedWeight) {
        assertThat(actualWeight, is(equalTo(decimal(expectedWeight))));
    }

}
