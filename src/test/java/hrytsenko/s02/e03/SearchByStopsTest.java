package hrytsenko.s02.e03;

import static org.hamcrest.CoreMatchers.equalTo;
import static org.hamcrest.CoreMatchers.is;
import static org.junit.Assert.assertThat;

import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.Optional;

import org.junit.Before;
import org.junit.Test;

public class SearchByStopsTest {

    private FlightService service;

    @Before
    public void init() {
        service = new FlightService();
    }

    @Test
    public void noFlights() {
        List<Flight> flights = Collections.emptyList();

        Optional<Flight> flight = service.search("KBP", "AMS", flights);

        assertThat(routeOf(flight), is(equalTo("")));
    }

    @Test
    public void noConnections() {
        List<Flight> flights = Arrays.asList(nonStop("KBP", "AMS"), nonStop("VIE", "FRA"));

        Optional<Flight> flight = service.search("KBP", "FRA", flights);

        assertThat(routeOf(flight), is(equalTo("")));
    }

    @Test
    public void oneConnection_nonStop() {
        List<Flight> flights = Arrays.asList(nonStop("KBP", "FRA"));

        Optional<Flight> flight = service.search("KBP", "FRA", flights);

        assertThat(routeOf(flight), is(equalTo("KBP:FRA")));
    }

    @Test
    public void oneConnection_oneStop() {
        List<Flight> flights = Arrays.asList(nonStop("KBP", "AMS"), nonStop("AMS", "FRA"));

        Optional<Flight> flight = service.search("KBP", "FRA", flights);

        assertThat(routeOf(flight), is(equalTo("KBP:AMS:FRA")));
    }

    @Test
    public void twoWayConnections() {
        List<Flight> flights = Arrays.asList(nonStop("KBP", "AMS"), nonStop("AMS", "KBP"), nonStop("AMS", "FRA"),
                nonStop("FRA", "AMS"));

        Optional<Flight> flight = service.search("KBP", "FRA", flights);

        assertThat(routeOf(flight), is(equalTo("KBP:AMS:FRA")));
    }

    @Test
    public void severalConnections_equalLength_chooseFirst() {
        List<Flight> flights = Arrays.asList(nonStop("KBP", "AMS"), nonStop("KBP", "VIE"), nonStop("AMS", "FRA"),
                nonStop("VIE", "FRA"));

        Optional<Flight> flight = service.search("KBP", "FRA", flights);

        assertThat(routeOf(flight), is(equalTo("KBP:AMS:FRA")));
    }

    @Test
    public void severalConnections_differentLength_chooseShortest() {
        List<Flight> flights = Arrays.asList(nonStop("KBP", "AMS"), nonStop("AMS", "FRA"), nonStop("KBP", "FRA"));

        Optional<Flight> flight = service.search("KBP", "FRA", flights);

        assertThat(routeOf(flight), is(equalTo("KBP:FRA")));
    }

    public static Flight nonStop(String from, String to) {
        return new Flight(from, to, Collections.emptyList());
    }

    public static String routeOf(Optional<Flight> flight) {
        return String.join(":", flight.map(Flight::stops).orElse(Collections.emptyList()));
    }

}
