package hrytsenko.s02.e01;

import static java.util.Arrays.asList;
import static org.hamcrest.CoreMatchers.equalTo;
import static org.hamcrest.CoreMatchers.is;
import static org.junit.Assert.assertThat;

import java.util.List;
import java.util.Map;

import org.junit.Before;
import org.junit.Test;

public class FindTopicsTest {

    private static final String TECHNOLOGY = "Technology";
    private static final String HEALTHCARE = "Healthcare";

    private static final String JOHN = "john";
    private static final String BART = "bart";
    private static final String MARK = "mark";

    private SubscriberService service;

    @Before
    public void init() {
        service = new SubscriberService();
    }

    @Test
    public void oneNewsletter_oneSubscriber() {
        Newsletter technology = newsletter(TECHNOLOGY, JOHN);

        Map<String, List<String>> topics = service.findTopics(asList(technology));

        assertTopics(topics.get(JOHN), TECHNOLOGY);
    }

    @Test
    public void oneNewsletter_severalSubscribers() {
        Newsletter technology = newsletter(TECHNOLOGY, JOHN, BART);

        Map<String, List<String>> topics = service.findTopics(asList(technology));

        assertTopics(topics.get(JOHN), TECHNOLOGY);
        assertTopics(topics.get(BART), TECHNOLOGY);
    }

    @Test
    public void severalNewsletters_oneSubscriber() {
        Newsletter technology = newsletter(TECHNOLOGY, JOHN);
        Newsletter healthcare = newsletter(HEALTHCARE, JOHN);

        Map<String, List<String>> topics = service.findTopics(asList(technology, healthcare));

        assertTopics(topics.get(JOHN), TECHNOLOGY, HEALTHCARE);
    }

    @Test
    public void severalNewsletters_severalSubscribers() {
        Newsletter technology = newsletter(TECHNOLOGY, JOHN, BART);
        Newsletter healthcare = newsletter(HEALTHCARE, JOHN, MARK);

        Map<String, List<String>> topics = service.findTopics(asList(technology, healthcare));

        assertTopics(topics.get(JOHN), TECHNOLOGY, HEALTHCARE);
        assertTopics(topics.get(BART), TECHNOLOGY);
        assertTopics(topics.get(MARK), HEALTHCARE);
    }

    private Newsletter newsletter(String topic, String... subscribers) {
        Newsletter newsletter = new Newsletter();
        newsletter.setTopic(topic);
        newsletter.setSubscribers(asList(subscribers));
        return newsletter;
    }

    private void assertTopics(List<String> actualTopics, String... expectedTopics) {
        assertThat(actualTopics, is(equalTo(asList(expectedTopics))));
    }

}
